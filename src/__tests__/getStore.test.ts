import getStore, {
  appReducer,
  initialState,
  appStartAction,
  gameStartAction,
  gameEndAction,
  IGameEndAction,
  toNextRoundAction,
  IToNextRoundAction,
  gameRoundLimit,
} from '../getStore';
import { GameStateTypes, GameUnitTypes, GamePlayerTypes } from '../types';

describe('redux related tests', () => {
  it('reducer should return initial state', () => {
    const state = appReducer(undefined, { type: 'something' });
    expect(state).toEqual(initialState);
  });

  it('reducer should return started state', () => {
    const testAction = appStartAction();
    const expectedState = {
      ...initialState,
      started: true,
    };
    const state = appReducer(initialState, testAction);
    expect(state).toEqual(expectedState);
  });

  it('creates redux store without crashing', () => {
    const store = getStore();
    expect(store.getState).toBeDefined();
    expect(store.dispatch).toBeDefined();
    expect(store.subscribe).toBeDefined();
  });

  it('reducer should return game started state after gamestart action', () => {
    const testPayload = [GameUnitTypes.ARCHER, GameUnitTypes.ARCHER, GameUnitTypes.CAVALRY, GameUnitTypes.PIKEMAN];
    const testAction = gameStartAction(testPayload);
    const inputState = {
      ...initialState,
      started: true,
    };
    const expectedState = {
      ...inputState,
      gameState: GameStateTypes.PLAYING,
      gameRound: 1,
      playerAScore: 0,
      playerBScore: 0,
      cpuUnitProbabilityArray: testPayload,
    };
    const state = appReducer(inputState, testAction);
    expect(state).toEqual(expectedState);
  });

  it('reducer should return game ended state after gameend action', () => {
    const testPayload = [9, 8];
    const testAction = gameEndAction(testPayload);
    const inputState = {
      ...initialState,
      started: true,
      gameState: GameStateTypes.PLAYING,
      gameRound: 20,
    };
    const expectedState = {
      ...inputState,
      gameState: GameStateTypes.ENDED,
      gameRound: gameRoundLimit,
      playerAScore: testPayload[0],
      playerBScore: testPayload[1],
    };
    const state = appReducer(inputState, testAction);
    expect(state).toEqual(expectedState);
  });

  it('reducer should return next rount state after next round action', () => {
    const testPayload = [9, 8];
    const testAction = toNextRoundAction(testPayload);
    const inputState = {
      ...initialState,
      started: true,
      gameState: GameStateTypes.PLAYING,
      gameRound: 8,
    };
    const expectedState = {
      ...inputState,
      gameRound: inputState.gameRound + 1,
      playerAScore: testPayload[0],
      playerBScore: testPayload[1],
    };
    const state = appReducer(inputState, testAction);
    expect(state).toEqual(expectedState);
  });
});
