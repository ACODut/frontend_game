import { runSaga } from 'redux-saga';
import { FIGHT, gameRoundLimit, TO_NEXT_ROUND, GAME_END } from '../getStore';
import { GameUnitTypes } from '../types';

import { listenFight } from '../sagas/GameSagas';

describe('test saga logic', () => {
  it('should go to next round with correct fight result', async () => {
    const dispatched = [];
    await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
        getState: () => ({ playerAScore: 0, playerBScore: 0, gameRound: 1 }),
      },
      listenFight,
      { type: FIGHT, payload: [GameUnitTypes.ARCHER, GameUnitTypes.CAVALRY] }
    );
    expect(dispatched).toEqual([{ type: TO_NEXT_ROUND, payload: [-1, 1] }]);
  });
  it('should end game with correct fight result', async () => {
    const dispatched = [];
    await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
        getState: () => ({ playerAScore: 0, playerBScore: 0, gameRound: gameRoundLimit }),
      },
      listenFight,
      { type: FIGHT, payload: [GameUnitTypes.ARCHER, GameUnitTypes.CAVALRY] }
    );
    expect(dispatched).toEqual([{ type: GAME_END, payload: [-1, 1] }]);
  });
});
