import * as GameEngine from '../GameEngine';
import { GameUnitTypes } from '../types';

describe('gameEngine test', () => {
  beforeEach(() => {
    jest.spyOn(Math, 'random').mockImplementation(() => 0.2);
  });

  let probabilityArray: GameUnitTypes[] = [];
  it('test getProbabilityArrayWithPreference', () => {
    probabilityArray = GameEngine.getProbabilityArrayWithPreference();
    expect(probabilityArray.length).toEqual(4);

    let archerCount = 0;
    let cavalryCount = 0;
    let pikemanCount = 0;
    probabilityArray.forEach((unittype) => {
      if (unittype === GameUnitTypes.ARCHER) {
        archerCount++;
      }
      if (unittype === GameUnitTypes.CAVALRY) {
        cavalryCount++;
      }
      if (unittype === GameUnitTypes.PIKEMAN) {
        pikemanCount++;
      }
    });

    expect(archerCount).toEqual(2);
    expect(cavalryCount).toEqual(1);
    expect(pikemanCount).toEqual(1);
  });

  it('test selectUnitWithProbabilityArray', async () => {
    const selected = await GameEngine.selectUnitWithProbabilityArray(probabilityArray);
    expect(selected).toEqual(GameUnitTypes.CAVALRY);
  });
});
