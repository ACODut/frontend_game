import { mount, shallow, ReactWrapper } from 'enzyme';
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import * as GameEngine from '../GameEngine';
import { initialState, gameStartAction } from '../getStore';
import App, { AppRoot, mapStateToProps } from '../App';
import { GameStateTypes, GameUnitTypes } from '../types';

const probabilityArray = [GameUnitTypes.ARCHER, GameUnitTypes.ARCHER, GameUnitTypes.PIKEMAN, GameUnitTypes.CAVALRY];
const sleep = (time) => new Promise((resolve) => setTimeout(resolve, time));

describe('Root App component rendering', () => {
  beforeEach(() => {
    jest.spyOn(GameEngine, 'getProbabilityArrayWithPreference').mockImplementation(() => probabilityArray);
    jest.spyOn(GameEngine, 'selectUnitWithProbabilityArray').mockImplementation((a) => {
      return Promise.resolve(GameUnitTypes.PIKEMAN);
    });
    jest.spyOn(React, 'useEffect').mockImplementation((f) => f());
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    const state = {
      ...initialState,
      started: true,
    };
    const props = mapStateToProps(state);
    ReactDOM.render(<AppRoot onStart={() => null} {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders loading state', () => {
    const props = {
      onStart: jest.fn(),
    };
    const state = {
      ...initialState,
      started: false,
    };
    const reduxtProps = mapStateToProps(state);
    const wrapper = shallow(<AppRoot {...props} {...reduxtProps} />);
    expect(wrapper.find('.loading').exists()).toBe(true);
    expect(wrapper.find('.app').exists()).toBe(true);
  });

  it('renders started state', () => {
    const props = {
      onStart: jest.fn(),
    };
    const state = {
      ...initialState,
      started: true,
    };
    const reduxtProps = mapStateToProps(state);
    const wrapper = shallow(<AppRoot {...props} {...reduxtProps} />);
    expect(wrapper.find('.loading').exists()).toBe(false);
    expect(wrapper.find('#startButton').exists()).toBe(true);
  });

  it('properly binds state', () => {
    const state = {
      ...initialState,
      started: true,
    };
    const props = mapStateToProps(state);
    expect(props.started).toEqual(true);
  });

  it('gameStart Action should be called after click start', () => {
    const state = {
      ...initialState,
      started: true,
    };
    let store = configureStore([])(state);
    store.dispatch = jest.fn();

    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );

    wrapper
      .find('#startButton')
      .at(0)
      .simulate('click');
    expect(store.dispatch).toHaveBeenCalledWith(gameStartAction(probabilityArray));
  });

  //local interaction test
  let appComponent: ReactWrapper;
  it('renders as expected after gamestart action dispatched', () => {
    const props = {
      onStart: jest.fn(),
    };
    const state = {
      ...initialState,
      started: true,
      gameState: GameStateTypes.PLAYING,
      gameRound: 1,
      playerAScore: 0,
      playerBScore: 0,
    };
    let store = configureStore([])(state);
    store.dispatch = jest.fn();

    appComponent = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );
    expect(appComponent.find('#UnitSelectDialog').exists()).toBe(true);
    expect(appComponent.find('#playerAScore').exists()).toBe(true);
    expect(appComponent.find('#playerAContent').exists()).toBe(true);
  });

  it('Should have correct unittype after select', async () => {
    expect(
      appComponent
        .find('#playerAContent')
        .at(0)
        .prop('unitType')
    ).toEqual(GameUnitTypes.NONE);
    appComponent
      .find('#CAVALRY')
      .at(0)
      .simulate('click');
    expect(
      appComponent
        .find('#playerAContent')
        .at(0)
        .prop('unitType')
    ).toEqual(GameUnitTypes.CAVALRY);
    // await sleep(1000);
    // expect(appComponent.find('#UnitSelectDialog').exists()).toBe(false);
    // expect(appComponent.find('#vsImage').exists()).toBe(true);
    // expect(appComponent.find('#playerBContent').at(0).prop('unitType')).toEqual(GameUnitTypes.CAVALRY);
  });
});
