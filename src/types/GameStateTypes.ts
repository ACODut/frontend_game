export enum GameStateTypes {
  WAITING = 'WAITING',
  PLAYING = 'PLAYING',
  ENDED = 'ENDED',
}
