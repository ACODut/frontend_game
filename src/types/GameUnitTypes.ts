export enum GameUnitTypes {
  NONE = 'NONE',
  CAVALRY = 'CAVALRY',
  PIKEMAN = 'PIKEMAN',
  ARCHER = 'ARCHER',
}
