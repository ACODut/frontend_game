import * as React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import './App.css';
import {
  IAppState,
  isAppStarted,
  appStartAction,
  gameStartAction,
  gameEndAction,
  toNextRoundAction,
  fightAction,
  gameRoundLimit,
} from './getStore';
import { GameUnitTypes, GameStateTypes } from './types';
import { UnitSelectDialog } from './components/UnitSelectDialog';
import { PlayerComponent } from './components/PlayerComponent';
import * as GameEngine from './GameEngine';
import vsImg from './images/vs.jpeg';

export interface Props {
  started: boolean;
  gameState: GameStateTypes;
  gameRound: number;
  playerAScore: number;
  playerBScore: number;
  cpuUnitProbabilityArray: Array<GameUnitTypes>;

  onStart: () => void;
  gameStart: (payload: Array<GameUnitTypes>) => void;
  gameEnd: (payload: Array<number>) => void;
  toNextRound: (payload: Array<number>) => void;
  fight: (payload: Array<GameUnitTypes>) => void;
}

export const AppRoot: React.FC<Props> = ({
  started,
  gameState,
  gameRound,
  playerAScore,
  playerBScore,
  cpuUnitProbabilityArray,
  onStart,
  gameStart,
  gameEnd,
  toNextRound,
  fight,
}) => {
  React.useEffect(() => {
    onStart();
  }, [onStart]);
  const [isFirstTurn, setIsFirstTurn] = React.useState(true);
  const [isFighting, setIsFighting] = React.useState(false);
  const [playerAUnit, setPlayerAUnit] = React.useState<GameUnitTypes>(GameUnitTypes.NONE);
  const [playerBUnit, setPlayerBUnit] = React.useState<GameUnitTypes>(GameUnitTypes.NONE);

  const selectUnit = async (unit: GameUnitTypes) => {
    if (isFirstTurn) {
      setIsFirstTurn(false);
      setPlayerAUnit(unit);
      let cpuUnit = await GameEngine.selectUnitWithProbabilityArray(cpuUnitProbabilityArray);
      setPlayerBUnit(cpuUnit);

      setIsFighting(true);
      await fightWithAnimation();
      setIsFighting(false);
      setPlayerAUnit(GameUnitTypes.NONE);
      setPlayerBUnit(GameUnitTypes.NONE);

      if (gameRoundLimit > gameRound) {
        setIsFirstTurn(true);
      }
      let selectedUnits: Array<GameUnitTypes> = [unit, cpuUnit];
      fight(selectedUnits);
    }
  };

  const fightWithAnimation = () => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 2000);
    });
  };

  const onSelectUnit = (unit: GameUnitTypes) => {
    selectUnit(unit);
  };

  const initState = () => {
    setIsFirstTurn(true);
    setIsFighting(false);
    setPlayerAUnit(GameUnitTypes.NONE);
    setPlayerBUnit(GameUnitTypes.NONE);
  };

  const onStartClick = () => {
    let probabilityArray = GameEngine.getProbabilityArrayWithPreference();
    gameStart(probabilityArray);
    initState();
  };

  if (!started) {
    return (
      <Grid container direction="row" justify="center" alignItems="center" className="app">
        <span className="loading">Loading...</span>
      </Grid>
    );
  }

  let gameContent = null;
  if (gameState === GameStateTypes.WAITING) {
    gameContent = (
      <Button variant="contained" onClick={onStartClick} key={'startButton'} id={'startButton'}>
        Start
      </Button>
    );
  }

  const playerAContent = <PlayerComponent unitType={playerAUnit} key={'playerAContent'} id={'playerAContent'} />;

  let playerBContent = null;
  if (!isFirstTurn) {
    playerBContent = <PlayerComponent unitType={playerBUnit} key={'playerBContent'} id={'playerBContent'} />;
  }

  let maybeVSImage = null;
  if (isFighting) {
    maybeVSImage = <img src={vsImg} key={'vsImage'} id={'vsImage'} />;
  }

  if (gameState === GameStateTypes.PLAYING) {
    gameContent = (
      <>
        <UnitSelectDialog open={isFirstTurn} onSelectUnit={onSelectUnit} />
        <Grid
          item
          container
          justify="center"
          alignItems="center"
          xs={4}
          className="scoreRow"
          key={'playerAScore'}
          id={'playerAScore'}>
          <Typography component="h2">PlayAScore:</Typography>
          <Typography component="h2">{playerAScore}</Typography>
        </Grid>
        <Grid
          item
          container
          justify="center"
          alignItems="center"
          xs={4}
          className="scoreRow"
          key={'gameRound'}
          id={'gameRound'}>
          <Typography component="h2">Round:</Typography>
          <Typography component="h2">{gameRound}</Typography>
        </Grid>
        <Grid
          item
          container
          justify="center"
          alignItems="center"
          xs={4}
          className="scoreRow"
          key={'playerBScore'}
          id={'playerBScore'}>
          <Typography component="h2">PlayBScore:</Typography>
          <Typography component="h2">{playerBScore}</Typography>
        </Grid>
        <Grid item container justify="center" alignItems="center" xs={5}>
          {playerAContent}
        </Grid>
        <Grid item container justify="center" alignItems="center" xs={2}>
          {maybeVSImage}
        </Grid>
        <Grid item container justify="center" alignItems="center" xs={5}>
          {playerBContent}
        </Grid>
      </>
    );
  }
  if (gameState === GameStateTypes.ENDED) {
    gameContent = (
      <>
        <Grid
          item
          container
          justify="center"
          alignItems="center"
          xs={4}
          className="scoreRow"
          key={'playerAScore'}
          id={'playerAScore'}>
          <Typography component="h2">PlayAScore:</Typography>
          <Typography component="h2">{playerAScore}</Typography>
        </Grid>
        <Grid
          item
          container
          justify="center"
          alignItems="center"
          xs={4}
          className="scoreRow"
          key={'gameRound'}
          id={'gameRound'}>
          <Typography component="h2">Round:</Typography>
          <Typography component="h2">{gameRound}</Typography>
        </Grid>
        <Grid
          item
          container
          justify="center"
          alignItems="center"
          xs={4}
          className="scoreRow"
          key={'playerBScore'}
          id={'playerBScore'}>
          <Typography component="h2">PlayBScore:</Typography>
          <Typography component="h2">{playerBScore}</Typography>
        </Grid>
        <Grid item container justify="center" alignItems="center" xs={4} key={'gameEndedMsg'} id={'gameEndedMsg'}>
          <Typography component="h2">GameEnded, Please click Restart to start again!</Typography>
        </Grid>
        <Button variant="contained" onClick={onStartClick}>
          Restart
        </Button>
      </>
    );
  }
  return (
    <Grid container direction="row" justify="center" alignItems="center" className="app">
      {gameContent}
    </Grid>
  );
};

export const mapStateToProps = (state: IAppState) => {
  const started = isAppStarted(state);
  return {
    started,
    gameState: state.gameState,
    gameRound: state.gameRound,
    playerAScore: state.playerAScore,
    playerBScore: state.playerBScore,
    cpuUnitProbabilityArray: state.cpuUnitProbabilityArray,
  };
};

export const mapDispatchToProps = {
  onStart: appStartAction,
  gameStart: gameStartAction,
  gameEnd: gameEndAction,
  toNextRound: toNextRoundAction,
  fight: fightAction,
};

const App = connect(mapStateToProps, mapDispatchToProps)(AppRoot);
export default App;
