import * as React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import ArcheryImg from '../images/Archery.png';
import CavalryImg from '../images/Cavalry.png';
import PikemanImg from '../images/Pikeman.png';
import { GameUnitTypes } from '../types';

export interface PlayerComponentProps {
  unitType: GameUnitTypes;
  id: string;
}
export const PlayerComponent: React.FC<PlayerComponentProps> = ({ unitType, id }) => {
  switch (unitType) {
    case GameUnitTypes.NONE:
      return <CircularProgress id={id} />;
    case GameUnitTypes.ARCHER:
      return <img src={ArcheryImg} id={id} />;
    case GameUnitTypes.CAVALRY:
      return <img src={CavalryImg} id={id} />;
    case GameUnitTypes.PIKEMAN:
      return <img src={PikemanImg} id={id} />;
    default:
      return <CircularProgress id={id} />;
  }
};
