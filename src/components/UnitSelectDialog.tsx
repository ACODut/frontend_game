import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { blue } from '@material-ui/core/colors';
import { GameUnitTypes } from '../types';

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

export interface UnitSelectDialogProps {
  open: boolean;
  onSelectUnit: (value: GameUnitTypes) => void;
}

export function UnitSelectDialog(props: UnitSelectDialogProps) {
  const classes = useStyles();
  const { onSelectUnit, open } = props;

  const handleListItemClick = (value: GameUnitTypes) => {
    onSelectUnit(value);
  };

  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open} disableBackdropClick id={'UnitSelectDialog'}>
      <DialogTitle id="simple-dialog-title">Select Unit type</DialogTitle>
      <List>
        <ListItem
          button
          onClick={() => handleListItemClick(GameUnitTypes.ARCHER)}
          key={GameUnitTypes.ARCHER}
          id={GameUnitTypes.ARCHER}>
          <ListItemAvatar>
            <Avatar className={classes.avatar}></Avatar>
          </ListItemAvatar>
          <ListItemText primary={GameUnitTypes.ARCHER} />
        </ListItem>
        <ListItem
          button
          onClick={() => handleListItemClick(GameUnitTypes.CAVALRY)}
          key={GameUnitTypes.CAVALRY}
          id={GameUnitTypes.CAVALRY}>
          <ListItemAvatar>
            <Avatar className={classes.avatar}></Avatar>
          </ListItemAvatar>
          <ListItemText primary={GameUnitTypes.CAVALRY} />
        </ListItem>
        <ListItem
          button
          onClick={() => handleListItemClick(GameUnitTypes.PIKEMAN)}
          key={GameUnitTypes.PIKEMAN}
          id={GameUnitTypes.PIKEMAN}>
          <ListItemAvatar>
            <Avatar className={classes.avatar}></Avatar>
          </ListItemAvatar>
          <ListItemText primary={GameUnitTypes.PIKEMAN} />
        </ListItem>
      </List>
    </Dialog>
  );
}
