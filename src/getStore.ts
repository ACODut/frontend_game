import { Action, ActionCreator, applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { GameStateTypes, GameUnitTypes } from './types';
import sagas from './sagas';

// game consts
export const gameRoundLimit = 20;

// action type(s)
export const APP_START = 'APP_START';
export type APP_START = typeof APP_START;
export interface IAppStartAction extends Action<APP_START> {
  type: APP_START;
}

export const GAME_START = 'GAME_START';
export type GAME_START = typeof GAME_START;
export interface IGameStartAction extends Action<GAME_START> {
  type: GAME_START;
  payload: Array<GameUnitTypes>;
}

export const GAME_END = 'GAME_END';
export type GAME_END = typeof GAME_END;
export interface IGameEndAction extends Action<GAME_END> {
  type: GAME_END;
  payload: Array<number>;
}

export const TO_NEXT_ROUND = 'TO_NEXT_ROUND';
export type TO_NEXT_ROUND = typeof TO_NEXT_ROUND;
export interface IToNextRoundAction extends Action<TO_NEXT_ROUND> {
  type: TO_NEXT_ROUND;
  payload: Array<number>;
}

export const FIGHT = 'FIGHT';
export type FIGHT = typeof FIGHT;
export interface IFightAction extends Action<FIGHT> {
  type: FIGHT;
  payload: Array<GameUnitTypes>;
}

export type AppActions = IAppStartAction | IGameStartAction | IGameEndAction | IToNextRoundAction | IFightAction;

// action builder(s)
export const appStartAction: ActionCreator<IAppStartAction> = () => ({
  type: APP_START,
});
export const gameStartAction: ActionCreator<IGameStartAction> = (payload: Array<GameUnitTypes>) => ({
  type: GAME_START,
  payload: payload,
});
export const gameEndAction: ActionCreator<IGameEndAction> = (payload: Array<number>) => ({
  type: GAME_END,
  payload: payload,
});
export const toNextRoundAction: ActionCreator<IToNextRoundAction> = (payload: Array<number>) => ({
  type: TO_NEXT_ROUND,
  payload: payload,
});
export const fightAction: ActionCreator<IFightAction> = (payload: Array<GameUnitTypes>) => ({
  type: FIGHT,
  payload: payload,
});

// state definition
export interface IAppState {
  started: boolean;
  gameState: GameStateTypes;
  gameRound: number;
  playerAScore: number;
  playerBScore: number;
  cpuUnitProbabilityArray: Array<GameUnitTypes>;
}

export const initialState: IAppState = {
  started: false,
  gameState: GameStateTypes.WAITING,
  gameRound: 1,
  playerAScore: 0,
  playerBScore: 0,
  cpuUnitProbabilityArray: [],
};

// app reducer
export function appReducer(state: IAppState = initialState, action: AppActions | Action) {
  switch (action.type) {
    case APP_START:
      return {
        ...state,
        started: true,
      };
    case GAME_START:
      return {
        ...state,
        gameState: GameStateTypes.PLAYING,
        gameRound: 1,
        playerAScore: 0,
        playerBScore: 0,
        cpuUnitProbabilityArray: (action as IGameStartAction).payload,
      };
    case GAME_END:
      return {
        ...state,
        gameState: GameStateTypes.ENDED,
        gameRound: gameRoundLimit,
        playerAScore: (action as IGameEndAction).payload[0],
        playerBScore: (action as IGameEndAction).payload[1],
      };
    case TO_NEXT_ROUND:
      return {
        ...state,
        gameRound: state.gameRound + 1,
        playerAScore: (action as IToNextRoundAction).payload[0],
        playerBScore: (action as IToNextRoundAction).payload[1],
      };
    default:
      return state;
  }
}

// selectors
export const isAppStarted = (state: IAppState) => state.started;

export type AppStore = Store<IAppState, AppActions | Action>;

export default function getStore(): AppStore {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(appReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
  sagaMiddleware.run(sagas);
  return store;
}
