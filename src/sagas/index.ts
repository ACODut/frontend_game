import { all, takeLatest } from 'redux-saga/effects';
// action types
import { FIGHT } from '../getStore';
// sagas
import { listenFight } from './GameSagas';

// connect types to sagas
export default function* root() {
  yield all([takeLatest(FIGHT, listenFight)]);
}
