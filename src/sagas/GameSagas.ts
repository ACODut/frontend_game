import { put, select } from 'redux-saga/effects';
import { IAppState, IFightAction, gameRoundLimit, toNextRoundAction, gameEndAction } from '../getStore';
import { getFightResult } from '../GameEngine';

export function* listenFight(action: IFightAction) {
  // fight and evaluate scores
  const playerAScore = yield select((state: IAppState) => state.playerAScore);
  const playerBScore = yield select((state: IAppState) => state.playerBScore);
  const fightResult = getFightResult(action.payload, [playerAScore, playerBScore]);

  // decide wether next round or endgame
  const gameRound = yield select((state: IAppState) => state.gameRound);
  if (gameRound < gameRoundLimit) {
    yield put(toNextRoundAction(fightResult));
  } else {
    yield put(gameEndAction(fightResult));
  }
}
