import { GameUnitTypes } from './types';

export function getFightResult(unitTypes: Array<GameUnitTypes>, scores: Array<number>): Array<number> {
  if (unitTypes[0] === GameUnitTypes.ARCHER && unitTypes[1] === GameUnitTypes.CAVALRY) {
    return [scores[0] - 1, scores[1] + 1];
  }
  if (unitTypes[0] === GameUnitTypes.CAVALRY && unitTypes[1] === GameUnitTypes.ARCHER) {
    return [scores[0] + 1, scores[1] - 1];
  }
  if (unitTypes[0] === GameUnitTypes.ARCHER && unitTypes[1] === GameUnitTypes.PIKEMAN) {
    return [scores[0] + 1, scores[1] - 1];
  }
  if (unitTypes[0] === GameUnitTypes.PIKEMAN && unitTypes[1] === GameUnitTypes.ARCHER) {
    return [scores[0] - 1, scores[1] + 1];
  }
  if (unitTypes[0] === GameUnitTypes.CAVALRY && unitTypes[1] === GameUnitTypes.PIKEMAN) {
    return [scores[0] - 1, scores[1] + 1];
  }
  if (unitTypes[0] === GameUnitTypes.PIKEMAN && unitTypes[1] === GameUnitTypes.CAVALRY) {
    return [scores[0] + 1, scores[1] - 1];
  }
  if (unitTypes[0] === GameUnitTypes.NONE || unitTypes[1] === GameUnitTypes.NONE) {
    return scores;
  }
  return [scores[0] - 1, scores[1] - 1];
}

export const selectUnitWithProbabilityArray = (probabilityArray: Array<GameUnitTypes>) => {
  return new Promise((resolve: (value: GameUnitTypes) => void, reject) => {
    setTimeout(() => {
      resolve(probabilityArray[Math.floor(Math.random() * probabilityArray.length)]);
    }, 3000);
  });
};

export const getProbabilityArrayWithPreference = () => {
  //choose preference unit
  let preference = GameUnitTypes.ARCHER;
  let randomNum = Math.random();
  if (randomNum < 0.33) {
    preference = GameUnitTypes.ARCHER;
  } else if (randomNum < 0.66) {
    preference = GameUnitTypes.CAVALRY;
  } else {
    preference = GameUnitTypes.PIKEMAN;
  }

  // generate preference array
  let probabilityArray: Array<GameUnitTypes> = [];
  for (let unitType in GameUnitTypes) {
    if (unitType === 'NONE') continue;
    if (GameUnitTypes[preference] === unitType) {
      probabilityArray.push(preference);
    }
    probabilityArray.push(GameUnitTypes[unitType as keyof typeof GameUnitTypes]);
  }

  return probabilityArray;
};
